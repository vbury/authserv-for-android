require('../models/db')

let jwt = require('jsonwebtoken');
let User = require('../models/user')


module.exports.auth = (req, res) => {
    User.findOne({
        'email': req.body.email
    }, (err, user) => {
        if (err) throw err;

        if (!user) {
            res.json({
                success: false,
                message: 'Authentication failed.'
            })
        } else if (user) {
            user.comparePassword(req.body.password, function (err, isMatch) {
                if (err) throw err
                if (isMatch) {
                    let ByUser = {
                        _id: user._id,
                        lastname:user.lastname,
                        firstname:user.firstname,
                        email: user.email,
                        role: user.role,
                        access: {
                            addconnect: user.addconnect,
                            modconnect: user.access.modconnect,
                            delconnect: user.access.delconnect,
                            write: user.access.write
                        }
                    }
                    let token = jwt.sign(ByUser, SuperSecret, {
                        expiresIn: "30d"
                    });
                    console.log(user);
                    res.json({
                        success: true,
                        message: 'Enjoy your token!',
                        token: token,
                        _id: user._id,
                        lastname:user.lastname,
                        firstname:user.firstname,
                        email: user.email,
                        role: user.role,
                        access: {
                                addconnect: user.access.addconnect,
                                modconnect: user.access.modconnect,
                                delconnect: user.access.delconnect,
                                write: user.access.write
                        }
                    });
                } else {
                    res.json({
                        success: false,
                        message: 'Authentication failed.'
                    })
                }
            });
        }
    });
}

module.exports.verify = (req, res) => {
    let token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, SuperSecret, (err, decoded) => {
            if (err) {
                return res.json({
                    sucess: false,
                    message: 'Failed to Authentication token.'
                });

            } else {
                User.findOne({
                    _id: req.body.id
                }, (err, user) => {
                    if (err) throw err;

                    if (!user) {
                        res.json({
                            success: false,
                            message: 'Authentication failed.'
                        })
                    } else if (user) {
                        res.json({
                            success: true,
                            _id: user._id,
                            lastname:user.lastname,
                            firstname:user.firstname,
                            email: user.email,
                            role: user.role,
                            access: {
                                addconnect: user.access.addconnect,
                                modconnect: user.access.modconnect,
                                delconnect: user.access.delconnect,
                                write: user.access.write
                            }
                        });
                    } else {
                        res.json({
                            success: false,
                            message: 'Authentication failed.'
                        })
                    }

                })
            }
        });
    } else {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        })
    }
}

module.exports.checkToken = (req, res, next) => {
    let token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, SuperSecret, (err, decoded) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Failed to Authentication token.'
                });

            } else {
                req.decode = decoded;
                next();
            }
        });
    } else {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
}

module.exports.requireAdmin = (req, res, next) => {
    if(!req.decode || req.decode.role !== 'admin'){
        return res.json({
            success: false,
            message: 'This route can only be accessed by an administrator'
        });
    }
    next();
}

module.exports.requireUser = (req, res, next) => {
    if(req.decode && req.decode._id === req.params.id || req.decode.role === 'admin') {
        // ce que tu veux
        return next();
    }
    return res.json({
        success: false,
        message: 'Failed to Authentication'
    })
}