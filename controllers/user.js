require('../models/db')

let User = require('../models/user')

let bcrypt = require('bcryptjs');

const PORT = process.env.PORT || 5000

module.exports.getAll = (req, res) => {
    User.find((err, users) => {
        if (!users) {
            res.status(404)
            res.json({
                err: "No user found :("
            })
        }

        if (err) {
            res.status(500)
            return res.json({
                err: "An unexpect error happened"
            })
        }
        let allUsers = []
        for (let user of users) {
            let ByUser = {
                _id: user._id,
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                role: user.role,
                access: {
                    addconnect: user.access.addconnect,
                    modconnect: user.access.modconnect,
                    delconnect: user.access.delconnect,
                    write: user.access.write
                }
            }
            allUsers.push(ByUser)
        }
        return res.json(allUsers)
    })
}

module.exports.getById = (req, res) => {
    User.findById(req.params.id, (err, user) => {
        if (user) {
            let ByUser = {
                _id: user._id,
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                role: user.role,
                access: {
                    addconnect: user.access.addconnect,
                    modconnect: user.access.modconnect,
                    delconnect: user.access.delconnect,
                    write: user.access.write
                }
            }
            console.log(ByUser);
            return res.json(ByUser)
        }

        if (err) {
            res.status(404)
            return res.json({
                error: "No user found for that id."
            })
        }
    })
}

module.exports.addUser = (req, res) => {
    console.log(req.body);
    User.findOne({
        'email': req.body.email
    }, (err, user) => {
        if (user) {
            return res.json({
                email: true
            })
        } else {
            let password = req.body.password
            bcrypt.genSalt(Salt, function (err, salt) {
                bcrypt.hash(password, salt, function (err, hash) {
                    req.body.password = hash
                    req.body.role = "user"
                    let user = User(req.body)
                    user.save((err, user) => {
                        if (err) {
                            console.log(err);
                            res.status(406)
                            return res.json({
                                error: "Could not create this user"
                            })
                        }

                        res.status(200)
                        return res.json({
                            success: true
                        })
                    })
                });
            });
        }
    })
}

module.exports.delUser = (req, res) => {
    User.findOneAndRemove({
        '_id': req.params.id
    }, (err, user) => {
        if (err) {
            res.status(500)
            return res.json('Could not remove this user :(')
        }

        res.status(200)
        return res.json({
            message: "User delete with success"
        })
    })
}

module.exports.updateUser = (req, res) => {
    User.findById(req.params.id, (err, user) => {
        if (user.lastname == req.body.lastname && user.email == req.body.email && user.firstname == req.body.firstname) {
            if (err) {
                res.status(500)
                return res.json({
                    error: 'Could not update this user'
                })
            }
            res.status(200)
            return res.json({
                message: "Do sommething",
            })
        } else if (user.email != req.body.email) {
            User.findOne({
                'email': req.body.email
            }, (err, user) => {
                if (user) {
                    return res.json({
                        error: "Email already use."
                    })
                } else {
                    User.update({
                        '_id': req.params.id
                    }, {
                        $set: {
                            'email': req.body.email,
                            'firstname': req.body.firstname,
                            'lastname': req.body.lastname
                        }
                    }, (err, user) => {
                        if (err) {
                            res.status(500)
                            return res.json({
                                error: 'Could not update this user'
                            })
                        }
                        res.status(200)
                        return res.json({
                            message: "User updated with success",
                            user
                        })
                    })
                }
            })
        } else {
            User.update({
                '_id': req.params.id
            }, {
                $set: {
                    'firstname': req.body.firstname,
                    'lastname': req.body.lastname
                }
            }, (err, user) => {
                if (err) {
                    res.status(500)
                    return res.json({
                        error: 'Could not update this user'
                    })
                }
                res.status(200)
                return res.json({
                    message: "User updated with success",
                    user
                })
            })
        }
    })
}

module.exports.updateLaw = (req, res) => {
    User.findOne({
        '_id': req.params.id
    }, (err, user) => {
        if (err) {
            res.status(500)
            return res.json({
                error: 'Could not update this user'
            })
        }
        if (user) {
            User.update({
                '_id': req.params.id
            }, {
                $set: {
                    'role': req.body.role,
                    'access': {
                        'addconnect': req.body.addconnect,
                        'modconnect': req.body.modconnect,
                        'delconnect': req.body.delconnect,
                        'write': req.body.write
                    }
                }
            }, (err, user) => {
                if (err) {
                    res.status(500)
                    return res.json({
                        error: 'Could not update this user'
                    })
                }
                res.status(200)
                return res.json({
                    message: "User updated with success",
                    user
                })
            })
        }

    })
}

module.exports.updatePass = (req, res) => {
    User.findOne({
        '_id': req.params.id
    }, (err, user) => {
        if (err) throw err

        user.comparePassword(req.body.password, function (err, isMatch) {
            if (err) throw err
            if (isMatch) {
                let password = req.body.newPassword
                bcrypt.genSalt(Salt, function (err, salt) {
                    bcrypt.hash(password, salt, function (err, hash) {
                        user.password = hash
                        user.save()
                        res.status(200)
                        return res.json({
                            message: "Password updated with success"
                        })
                    })
                })
            } else {
                res.status(500)
                return res.json({
                    message: "Bad password"
                })
            }
        });

    })
}