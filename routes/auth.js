let router = require('express').Router();
let ctrl = require('../controllers/auth');

router.post('/', ctrl.auth);
router.post('/verify', ctrl.verify)

module.exports = router;