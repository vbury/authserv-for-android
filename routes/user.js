let router = require('express').Router()
let ctrl = require('../controllers/user')
let auth = require('../controllers/auth')

const authMixin = [auth.checkToken, auth.requireAdmin]
const authUser = [auth.checkToken, auth.requireUser]

router.get('/', authMixin, ctrl.getAll)
router.post('/', ctrl.addUser)
router.get('/:id',authUser, ctrl.getById)
router.delete('/:id', authUser, ctrl.delUser)
router.put('/:id', authUser, ctrl.updateUser)
router.post('/changePass/:id',authUser, ctrl.updatePass)
router.put('/law/:id',authMixin, ctrl.updateLaw)

module.exports = router;
