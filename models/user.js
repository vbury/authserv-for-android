let mongoose = require('mongoose')
let bcrypt = require('bcryptjs');

let userSchema = new mongoose.Schema({
    
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email:{
        type: String,
        required:true,
        unique: true
    },
    role: {
        type: String,
        required: true,
        default: "user"
    },
    access:{
        addconnect:{
            type:Boolean, 
            required:true,
            default:false
        },
        modconnect:{
            type:Boolean,
            required:true,
            default:false
        },
        delconnect:{
            type:Boolean,
            required:true,
            default:false
        },
        write:{
            type:Boolean,
            required:true,
            default:false
        }
    },
    createdAt: {
		type: Date,
		default: Date.now
	}
});

userSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', userSchema)